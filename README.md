# ESP8266 Blynk Relay switch for AC or DC
An Blynk enabled WiFi AC/DC power switch for lazy lads.

# Requirements
- Arduino Uno
- ESP8266
- Arduino IDE
- A relay

# Instruction
## Program your ESP8266 so it can connect to a Blynk server.
1. Connect your ESP8266 to your Arduino Uno as shown in the diagram below, then plug Arduino Uno into your computer.
![arduino_esp8266_bb](arduino_esp8266_bb.png)

2. Start the Arduino IDE.

3. Go to **File > Preferences**

4. Add the below link to **Additional Boards Manager URLs**
`http://arduino.esp8266.com/stable/package_esp8266com_index.json`

5. Go to **Tools > Boards > Boards Manager...**

6. Search **ESP8266**, then click **Install** to install ESP8266 board.

7. Close the Boards Manager window and select **Generic ESP8266 Module** from the board selection list.

8. Load [main/main.io] and **Upload**.

## Circuit time
1. Connect your ESP8266 and relay as shown in the diagram below. *I'm supplying 5v to my chip here because it works fine with 5v, although it's suggested 3.3v to be used. You can use a linear voltage regulator or get a 3.3v power supply to solve the issue*.
![esp8266_relay](esp8266_relay_bb.png)
